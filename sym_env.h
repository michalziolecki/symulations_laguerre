#ifndef LAGUERRE_SYM_ENV_H
#define LAGUERRE_SYM_ENV_H

#include <iostream>
#include <vector>
#include <complex>
#include <utility>
#include <map>
#include <math.h>
#include <regex>
#include <string>
#include <chrono>
#include <thread>
#include <csignal>

// project used types
typedef std::complex<double> complex_;
typedef std::vector<std::complex<double>> complex_v;
typedef std::vector<std::pair<std::string, std::complex<double>>> complex_v_p;
typedef std::map<std::string, std::complex<double>> complex_m;

// project algorithms
complex_m ddhorner(complex_v &func_param, complex_ &started_point);

complex_m laguerre(complex_v &func_param, complex_ &started_point,
                   complex_ &delta, complex_ &max_limit);

#endif //LAGUERRE_SYM_ENV_H
