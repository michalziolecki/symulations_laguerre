#include "sym_env.h"


void get_params(std::string &input) {
    std::cout << "\n Get params [ex: '([1,0,1,-7], 3i, 0.01, 100)' ]"
                 " or write 'exit' to finish: \n";
    std::getline(std::cin, input);
    std::cout << "\n input: " << input << "\n \n";
}

bool create_complex_nb(complex_ &number, const std::string &tmp_val) {
    bool create_complex_stat = true;
    size_t found_plus = tmp_val.find('+');

    if (found_plus != std::string::npos) {
        std::string real_val = tmp_val.substr(0, found_plus);
        std::string img_val = tmp_val.substr(found_plus + 1);
        try {
            if (!real_val.empty()) {
                double real_d = std::stod(real_val);
                number.real(real_d);
            }
            if (!img_val.empty()) {
                size_t find_i = img_val.find('i');
                double img_d;
                if (find_i != std::string::npos) {
                    img_d = std::stod(img_val.substr(0, find_i));
                } else {
                    img_d = std::stod(img_val);
                }
                number.imag(img_d);
            }
        } catch (std::exception &e) {
            std::cout << "Bad cast input to complex - probably wrong input !" << std::endl;
            create_complex_stat = false;
        }

    } else {
        size_t find_i = tmp_val.find('i');
        try {
            if (find_i != std::string::npos) {
                double img_val = std::stod(tmp_val.substr(0, find_i));
                number.imag(img_val);
            } else {
                double real_val = std::stod(tmp_val);
                number.real(real_val);
            }
        } catch (std::exception &e) {
            std::cout << "Bad cast input to complex - probably wrong input !" << std::endl;
            create_complex_stat = false;
        }
    }
    return create_complex_stat;
}

bool parse_params(std::string &input, complex_v &function_params,
                  complex_ &started_point, complex_ &delta, complex_ &max_limit) {

    bool params_st = false;
    bool function_params_st = false;
    bool params_end = false;
    bool function_params_end = false;
    bool final_status = true;
    int params = 0;
    int PARAM_NB = 4;
    std::string BAD_COMPLEX_CAST("Create complex finished bad status! \n");
    std::string reg_str(R"([i0-9\-\+\.]{1})");
    std::string tmp_val;
    std::regex v_regex(reg_str);
    std::smatch match;

    if (!input.empty()) {
        for (char &symbol : input) {
            if (symbol == '(') params_st = true;

            if (params_st) {
                if (symbol == '{' || symbol == '[') function_params_st = true;

                if (std::regex_match(std::string(1, symbol), v_regex)) {
                    tmp_val += symbol;
                }

                if (function_params_st) {
                    if (symbol == ',') {
                        complex_ cplx;
                        if (!create_complex_nb(cplx, tmp_val)) {
                            std::cout << BAD_COMPLEX_CAST;
                            final_status = false;
                            break;
                        }
                        function_params.push_back(cplx);
                        tmp_val.clear();
                    }
                    if (symbol == '}' || symbol == ']') {
                        function_params_st = false;
                        function_params_end = true;
                        complex_ cplx;
                        if (!create_complex_nb(cplx, tmp_val)) {
                            std::cout << BAD_COMPLEX_CAST;
                            final_status = false;
                            break;
                        }
                        function_params.push_back(cplx);
                        tmp_val.clear();
                    }
                }

                if (function_params_end && symbol == ',') {
                    params++;
                    if (params == 2) {
                        if (!create_complex_nb(started_point, tmp_val)) {
                            std::cout << BAD_COMPLEX_CAST;
                            final_status = false;
                            break;
                        }
                    } else if (params == 3) {
                        if (!create_complex_nb(delta, tmp_val)) {
                            std::cout << BAD_COMPLEX_CAST;
                            final_status = false;
                            break;
                        }
                    } else if (params > 3) std::cout << "To much params ! \n";
                    tmp_val.clear();
                }
                if (symbol == ')') {
                    params++;
                    if (params == 4) {
                        if (!create_complex_nb(max_limit, tmp_val)) {
                            std::cout << BAD_COMPLEX_CAST;
                            final_status = false;
                            break;
                        }
                    }
                    params_st = false;
                    params_end = true;
                    tmp_val.clear();
                }
            }
            if (params_end) break;
        }
    } else {
        std::cout << "Params were empty ! \n";
    }

    if (function_params.empty() || PARAM_NB != params) {
        std::cout << "Bad parameters input detected ! \n";
        final_status = false;
    }
    return final_status;
}

void show_result(const complex_m &returned_params) {
    std::cout << "pn = " << returned_params.find("pn")->second.real() <<
              " + " << returned_params.find("pn")->second.imag() << "i" << "\n";
    std::cout << "err = " << returned_params.find("err")->second.real() <<
              " + " << returned_params.find("err")->second.imag() << "i" << "\n";
    std::cout << "k = " << returned_params.find("k")->second.real() <<
              " + " << returned_params.find("k")->second.imag() << "i" << "\n";
}