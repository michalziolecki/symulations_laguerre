#include "sym_env.h"

/*
 * This function calculate derivatives of polynomial
 * @params - function parameters ex: {1,0,1} and started point ex: 3i
 * @return - map with pair - key (name of value) and values - y (polynomial value),
 * z (1st derivative), v (2nd derivative)
 * example of using: ddhorner({1,0,1,-7},3i);
 * */
complex_m ddhorner(complex_v &func_param, complex_ &started_point) {

    complex_m returned_params;
    int input_size = func_param.size();
    complex_ y = func_param.at(0);
    complex_ z = 0;
    complex_ v = 0;

    for (int i = 1; i < input_size; i++) {
        v = z + v * started_point;
        z = y + z * started_point;
        y = y * started_point + func_param.at(i);
    }
    v = v * (complex_) 2;

    returned_params.insert(std::make_pair("v", v));
    returned_params.insert(std::make_pair("y", y));
    returned_params.insert(std::make_pair("z", z));
    return returned_params;
}