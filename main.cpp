#include "sym_env.h"
#include "user_interface.h"

void signal_handler(int signal_num) {
    std::cout << "Tool ended by the interrupt signal " << signal_num << ". \n";
    exit(signal_num);
}

void tool_loop() {
    while (true) {
        std::string input;
        complex_v function_params;
        complex_ started_point;
        complex_ delta;
        complex_ max_limit;

        get_params(input);
        if (input == "exit") {
            std::cout << "*** Laguerre algorithm tool shutdown *** \n";
            std::this_thread::sleep_for(std::chrono::seconds(2));
            break;
        }
        bool status = parse_params(input, function_params,
                                   started_point, delta, max_limit);
        if (status) {
            complex_m returned_params = laguerre(function_params,
                                                 started_point, delta, max_limit);
            show_result(returned_params);
        } else std::cout << "Parsing input problem ! \n";

        input.clear();
    }
}

int main() {
    signal(SIGABRT, signal_handler);

    std::cout << "*** Welcome in Laguerre algorithm tool! *** \n";
    std::cout << "        Created by Michal Ziolecki \n";

    tool_loop();

    return 0;
}