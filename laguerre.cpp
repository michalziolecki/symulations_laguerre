#include "sym_env.h"

/*
 * This function calculate laguerre algorithm to find elements of polynomial
 * @params - function parameters ex: {1,0,1,-7} and started point ex: 3i,
 * precision value ex: 0.01, iteration limit
 * @return - map with pair - key (name of value) and values -
 * pn (approximate solution), k (number of steps), err (bad solution value)
 * example of using: laguerre({1,0,1,-7}, 3i, 0.01, 100);
 * */
complex_m laguerre(complex_v &func_param, complex_ &started_point,
                   complex_ &delta, complex_ &max_limit) {

    complex_ err = 0.0;
    complex_ pn = 0.0;
    int element_size = func_param.size() - 1;
    complex_m returned_params;
    int k;

    for (k = 0; k < func_param.size(); k++) {

        complex_m horner_result = ddhorner(func_param, started_point);
        complex_ v = horner_result.find("v")->second;
        complex_ y = horner_result.find("y")->second;
        complex_ z = horner_result.find("z")->second;

        if (y == 0.0) {
            err = 0.0;
            pn = 0.0;
            break;
        }

        complex_ a = (-z) / y;
        complex_ b = pow(a, 2.0) - (v / y);
        complex_ t = sqrt((complex_) (element_size - 1) * ((complex_) element_size * b - pow(a, 2.0)));
        complex_ c_one = (a + t) / (complex_) element_size;
        complex_ c = (a - t) / (complex_) element_size;

        if (std::abs(c_one) > std::abs(c)) {
            c = c_one;
        }

        pn = started_point + ((complex_) 1 / c);
        err = abs(pn - started_point);
        started_point = pn;
        if (delta.real() > err.real() || delta.imag() > err.imag()) break;
        if (max_limit.real() < k) break;
    }

    returned_params.insert(std::make_pair("err", err));
    returned_params.insert(std::make_pair("pn", pn));
    returned_params.insert(std::make_pair("k", (complex_) (k + 1)));
    return returned_params;
}
