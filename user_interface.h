#ifndef LAGUERRE_USER_INTERFACE_H
#define LAGUERRE_USER_INTERFACE_H

#include "sym_env.h"

// User interface layer
void get_params(std::string &input);
bool parse_params(std::string &input, complex_v &function_params,
                  complex_ &started_point, complex_ &delta, complex_ &max_limit);
void show_result(const complex_m &returned_params);
bool create_complex_nb(complex_ &number, const std::string &tmp_val);

#endif //LAGUERRE_USER_INTERFACE_H
